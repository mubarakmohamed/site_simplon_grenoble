$(document).ready(function() {
    // easyAdmin datepicker config
    $('.datepicker').datepicker({
        language: 'fr',
        buttonImageOnly: true,
        showOn: 'button',
        orientation: "auto",
        autoclose: true,
        changeMonth: true,
        changeYear: true,
        format: 'yyyy-mm-dd'
    });

    // add span and icon to link
    addIconToLink();

    // add icon in dropDown menu
    $( ".select2-selection--single" ).on('click', function() {
        addIconToSelect(false);
    });
    $('#event_icon').change(function() {
        addIconToSelect(true);
    });

    if ( $( ".event-icon" ).length ) {
        $( ".event-icon" ).each(function() {
            var text = $.trim($(this).text());
            if (text != "Icône") {
                $(this).html("<i class='fa fa-"+text+"' aria-hidden='true'></i>");
            }
        });
    }
});

function addIconToLink() {
    var links = ["action-edit", "action-delete", "action-list", "action-save", "action-new"];
    var icons = ["pencil-square", "trash", "list", "check-square-o", "plus-square"];
    for (var i = 0; i < links.length; i++) {
        var link =  $('.'+links[i]);
        appendIconToElem(link, icons[i]);
    }
}

function addIconToSelect(isOnSelect) {
    if (isOnSelect) {
        var select = $('#select2-event_icon-container');
        appendIconToElem(select, null);
    }
    else {
        $('ul#select2-event_icon-results li').each(function (index, value) {
            var text = $(this).text();
            if (text != "Aucun(e)") {
                appendIconToElem($(this), null);
            }
        });
    }
}

function appendIconToElem(elem, className) {
    var text = elem.first().text();
    elem.empty();
    elem.append('<span class="icon-title">'+text+'</span>');
    if (className != null) {
        text = className;
    }
    elem.append("<i class='fa fa-"+text+"' aria-hidden='true'></i>");

}

function addIcon(elem) {
    var text = elem.first().text();
    elem.html("<i class='fa fa-"+text+"' aria-hidden='true'></i>")
}
