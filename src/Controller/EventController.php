<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Event;
use Symfony\Component\HttpFoundation\Request;



class EventController extends AbstractController
{
    // /**
    //  * @Route("/event", name="event")
    //  */
    // public function index()
    // {
    //     $event = $this->getDoctrine()->getRepository(Event::class);        

    //     return $this->render('event/index.html.twig', [
    //         'events' => $event->findBy([], ['id' => 'DESC'])
    //     ]);
    // }

    /**
     * @Route("/event/{id}", name="showEvent")
     */
    public function show($id)
    {
        $event = $this->getDoctrine()->getRepository(Event::class);
        $event = $event->findOneById($id);

        return $this->render('event/index.html.twig', [
            'event' => $event
        ]);
    }
}
