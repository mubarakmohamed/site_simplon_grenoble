Telecharger la branche devel_lionel

# Installation dépendance
composer install
npm i (si erreur virer le package-lock.json)

# build webpack
npm run dev

# creation database + migration + fixtures

changer le .env user, mdp, /!\ nom de la base de donnée != de l'ancienne

supprimer le(s) fichiers migrations src->Migrations

php bin/console doctrine:database:create
php bin/console make:migration
php bin/console doctrine:migrations:migrate

#Fixtures
(Si base de donnée existante supprimer toutes les tables dans phpmyadmmin (promo, student...).
Ne pas activer la checkbox pour vérifier les clé étrangère.
supprimer le(s) fichiers migrations src->Migrations
refaire 


php bin/console make:migration
php bin/console doctrine:migrations:migrate

)

php bin/console doctrine:fixtures:load

#Infos
route:
@Route("/accueil", name="home")
@Route("/a-propos", name="about")
@Route("/promos", name="promos")
@Route("/promos/{id}", name="promo-details")
@Route("/contact", name="contact")

backOffice:
/admin
user:admin
mpd:admin

# HAVE FUN!!
